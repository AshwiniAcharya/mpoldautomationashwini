package lib.dataObject;
import java.util.Random;

import lib.dataObject.DirectDepositData.ConsumerTag;

import qaframework.lib.UserDefinedFunction.ReadDataFromXMLFile;

public class DirectDepositData {
	public DirectDepositData() throws Exception {
		
	}
	ReadDataFromXMLFile xml = new ReadDataFromXMLFile();
	static final String FILEPATH = "DirectDepositDataXml";
	
	public final String employeeName = xml.read(ConsumerTag.employeeName, FILEPATH),
			                     emplyeeNameSecond = xml.read(ConsumerTag.emplyeeNameSecond, FILEPATH),
			                     ssn = xml.read(ConsumerTag.ssn, FILEPATH),
					             address = xml.read(ConsumerTag.address, FILEPATH),
							     city = xml.read(ConsumerTag.city, FILEPATH),
								 zip = xml.read(ConsumerTag.zip, FILEPATH),
								 phone = xml.read(ConsumerTag.phone, FILEPATH),
								 bankName = xml.read(ConsumerTag.bankName, FILEPATH),
								 accountName = xml.read(ConsumerTag.accountName, FILEPATH),
								 route = xml.read(ConsumerTag.route, FILEPATH),
								 phoneNumber = xml.read(ConsumerTag.phoneNumber, FILEPATH),
								 bankAddress = xml.read(ConsumerTag.bankAddress, FILEPATH),								 
								 cameraRoll = xml.read(ConsumerTag.cameraRoll, FILEPATH),
		                          photoNameiOS11 = xml.read(ConsumerTag.photoNameiOS11, FILEPATH),
		                          photoNameiOS10 = xml.read(ConsumerTag.photoNameiOS10, FILEPATH),
		                        	 alertForSubmittedReport = xml.read(ConsumerTag.alertForSubmittedReport, FILEPATH),
		                        	 OK = xml.read(ConsumerTag.OK, FILEPATH);
	
	public static class ConsumerTag {
		public static final String employeeName = "employeeName",
				emplyeeNameSecond = "emplyeeNameSecond",
				ssn = "ssn",
				address = "address",
				city = "city",
				zip = "zip",
				phone = "phone",
				bankName = "bankName",
				accountName = "accountName",
				route = "route",
				phoneNumber = "phoneNumber",
				bankAddress = "bankAddress",
				cameraRoll = "cameraRoll",
				photoNameiOS11 = "photoNameiOS11",
				photoNameiOS10 = "photoNameiOS10",
				alertForSubmittedReport = "alertForSubmittedReport",
				OK = "OK";				
	}
}
