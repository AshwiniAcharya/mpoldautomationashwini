package lib.dataObject;

import lib.dataObject.MonthlyBluesheetData.ConsumerTag;
import qaframework.lib.UserDefinedFunction.ReadDataFromXMLFile;

public class MonthlyBluesheetData {
	public MonthlyBluesheetData() throws Exception {
		
	}
	ReadDataFromXMLFile xml = new ReadDataFromXMLFile();
	static final String FILEPATH = "MonthlyBluesheetXml";
	
	public final String unit = xml.read(ConsumerTag.unit, FILEPATH),
			                         unitSecond = xml.read(ConsumerTag.unitSecond, FILEPATH), 
			                         odometer = xml.read(ConsumerTag.odometer, FILEPATH),
			                         job = xml.read(ConsumerTag.job, FILEPATH),
			                         inspectedBy = xml.read(ConsumerTag.inspectedBy, FILEPATH),
			                         fullComment = xml.read(ConsumerTag.fullComment, FILEPATH), 
			                         trianglesGoodComment = xml.read(ConsumerTag.trianglesGoodComment, FILEPATH), 
			                         needMoreComment = xml.read(ConsumerTag.needMoreComment, FILEPATH), 
			                         miscComment = xml.read(ConsumerTag.miscComment, FILEPATH),  
			                        	alertForSubmit = xml.read(ConsumerTag.alertForSubmit, FILEPATH),
			                      	cameraRoll = xml.read(ConsumerTag.cameraRoll, FILEPATH),
		                          	photoNameiOS11 = xml.read(ConsumerTag.photoNameiOS11, FILEPATH),
		                             photoNameiOS10 = xml.read(ConsumerTag.photoNameiOS10, FILEPATH),
                                     OK = xml.read(ConsumerTag.OK, FILEPATH);
	
	public static class ConsumerTag {
		  public static final String unit = "unit",
				                                      unitSecond = "unitSecond",
				                                      odometer = "odometer",
				                                      job = "job",
				                                      inspectedBy = "inspectedBy",
				                                      fullComment = "fullComment",
				                                      trianglesGoodComment = "trianglesGoodComment",
				                                      needMoreComment = "needMoreComment",
				                                      miscComment = "miscComment",
				                                    	 alertForSubmit = "alertForSubmittedReport",
				                                      cameraRoll = "cameraRoll",
				                                      photoNameiOS11 = "photoNameiOS11",
				                                      photoNameiOS10 = "photoNameiOS10",
				                                      OK = "OK";			                                      
			}
}
