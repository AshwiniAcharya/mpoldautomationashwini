package lib.dataObject;

import lib.dataObject.WeeklyForemanData.ConsumerTag;
import qaframework.lib.UserDefinedFunction.ReadDataFromXMLFile;

public class WeeklyForemanData {
    public WeeklyForemanData() throws Exception {
		
	}
    ReadDataFromXMLFile xml = new ReadDataFromXMLFile();
	static final String FILEPATH = "WeeklyForemanXml";
	
	public final String OK = xml.read(ConsumerTag.OK, FILEPATH),			
			alertForSubmit = xml.read(ConsumerTag.alertForSubmit, FILEPATH);
	
	public static class ConsumerTag {
		public static final String 	OK = "OK",			   
				alertForSubmit = "alertForSubmit";				
	}

}
