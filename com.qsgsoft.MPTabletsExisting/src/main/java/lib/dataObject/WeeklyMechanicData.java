package lib.dataObject;

import qaframework.lib.UserDefinedFunction.ReadDataFromXMLFile;

public class WeeklyMechanicData {
    public WeeklyMechanicData() throws Exception {
		
	}
	ReadDataFromXMLFile xml = new ReadDataFromXMLFile();
	static final String FILEPATH = "WeeklyMechanicXml";
	
	public final String company = xml.read(ConsumerTag.company, FILEPATH),
			secondCompany = xml.read(ConsumerTag.secondCompany, FILEPATH),
			remarks = xml.read(ConsumerTag.remarks, FILEPATH),
			empName = xml.read(ConsumerTag.empName, FILEPATH),
			secondEmpName = xml.read(ConsumerTag.secondEmpName, FILEPATH),
			EmpName2TC = xml.read(ConsumerTag.EmpName2TC, FILEPATH),
			EmpName2TC2 = xml.read(ConsumerTag.EmpName2TC2, FILEPATH),
			stHours = xml.read(ConsumerTag.stHours, FILEPATH),
			stjob = xml.read(ConsumerTag.stjob, FILEPATH),
		    stwo = xml.read(ConsumerTag.stwo, FILEPATH),
			otHours = xml.read(ConsumerTag.otHours, FILEPATH),
			secondStHours = xml.read(ConsumerTag.secondStHours, FILEPATH),
			secondOtHours = xml.read(ConsumerTag.secondOtHours, FILEPATH),
			secondDtHours = xml.read(ConsumerTag.secondDtHours, FILEPATH),
			otJob = xml.read(ConsumerTag.otJob, FILEPATH),
			otwo = xml.read(ConsumerTag.otwo, FILEPATH),
			dtHours = xml.read(ConsumerTag.dtHours, FILEPATH),
			dtJob = xml.read(ConsumerTag.dtJob, FILEPATH),
			dtWo = xml.read(ConsumerTag.dtWo, FILEPATH),
		    perDiem = xml.read(ConsumerTag.perDiem, FILEPATH),
			perDiemSecond = xml.read(ConsumerTag.perDiemSecond, FILEPATH),
			OK = xml.read(ConsumerTag.OK, FILEPATH),
			returnEmp = xml.read(ConsumerTag.returnEmp, FILEPATH),
			monEquipment = xml.read(ConsumerTag.monEquipment, FILEPATH),
			alertForSubmit = xml.read(ConsumerTag.alertForSubmit, FILEPATH);

	
	public static class ConsumerTag {
		public static final String company = "company",
				secondCompany = "secondCompany",
				EmpName2TC = "EmpName2TC",
				EmpName2TC2 = "EmpName2TC2",
				remarks = "remarks",
				empName = "empName",
				secondEmpName = "secondEmpName",
				secondDtHours = "secondDtHours",
				secondOtHours = "secondOtHours",
				stHours = "stHours",
				stjob = "stjob",
				stwo = "stwo",
				otHours = "otHours",
				otJob = "otJob",
				otwo = "otwo",
				dtHours = "dtHours",
				secondStHours = "secondStHours",
				dtJob = "dtJob",
				dtWo = "dtWo",
				perDiem = "perDiem",
				perDiemSecond = "perDiemSecond",
				OK = "OK",
			    returnEmp = "returnEmp",
				monEquipment = "monEquipment",
				alertForSubmit = "alertForSubmit";				
	}
}
