package lib.dataObject;
import java.util.Random;

import lib.dataObject.AtvSafetyData.ConsumerTag;
import qaframework.lib.UserDefinedFunction.ReadDataFromXMLFile;

public class AtvSafetyData {
     public AtvSafetyData() throws Exception {
		
	}
	ReadDataFromXMLFile xml = new ReadDataFromXMLFile();
	static final String FILEPATH = "AtvSafetyDataXml";
	
	public final String employeeName = xml.read(ConsumerTag.employeeName, FILEPATH),
			emplyeeNameSecond = xml.read(ConsumerTag.emplyeeNameSecond, FILEPATH),
			alertForSubmittedReport = xml.read(ConsumerTag.alertForSubmittedReport, FILEPATH),
			done = xml.read(ConsumerTag.done, FILEPATH),
       	    OK = xml.read(ConsumerTag.OK, FILEPATH);
	
	public static class ConsumerTag {
		public static final String employeeName = "employeeName",
				emplyeeNameSecond = "emplyeeNameSecond",
				alertForSubmittedReport = "alertForSubmittedReport",
				done = "done",
				OK = "OK";
	}

}
