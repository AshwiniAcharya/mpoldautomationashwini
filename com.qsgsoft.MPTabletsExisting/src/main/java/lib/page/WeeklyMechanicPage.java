package lib.page;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.HideKeyboardStrategy;
import lib.locators.CreateExpense.Locators;
import lib.locators.CreateProjectSite.ProjectSiteLocators;
import lib.locators.CreateWeeklyMechanic.WeeklyMechanicLocators;
import qaframework.Configuration.Config_MobileAndWeb;
import qaframework.WebElement.WebTimeConstant;
import qaframework.custom.WaitForElement;

public class WeeklyMechanicPage {
	WaitForElement wait;
	IOSDriver<MobileElement> driver;
	
	public WeeklyMechanicPage(IOSDriver<MobileElement> _driver) throws Exception {
		super();
		this.driver = _driver;
		wait = new WaitForElement(this.driver);
	}
	
	public WebDriver wdriver = Config_MobileAndWeb.wdriver;
	
	/**********************************************************************************
	 * Description : This function used click on the element by the name
	 *  Date : 15-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public WeeklyMechanicPage clickElementByName(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to click on element using xpath
	 *  Date :15-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public WeeklyMechanicPage clickElementByxpath(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		 return this;
	}
	
	
	public WeeklyMechanicPage clickElementByxpathClass(String locators) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
		driver.findElement(By.xpath("//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[4]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeCollectionView[1]/XCUIElementTypeCell[12]")).click();
	    return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in Incident
	 *  Date : 15-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public WeeklyMechanicPage enterValueInWeeklyMechanic(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);		

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		driver.findElement(By.name(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date in by scrolling
	 * Date : 15-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public WeeklyMechanicPage clickSubmit(String locator) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
        driver.findElement(By.name(locator)).click();
		return this;

	  }	
	
	/**********************************************************************************
	 * Description : This function used to verify alert msg for submitted report :
	 * 15-May-2018 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public WeeklyMechanicPage verReportIsSubmitted(String msg) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(WeeklyMechanicLocators.alertForSubmit)));
		driver.findElement(By.name(WeeklyMechanicLocators.alertForSubmit)).isDisplayed();
		String actual = driver.findElement(By.name(WeeklyMechanicLocators.alertForSubmit)).getText();
		System.out.println("receive" + actual);
		assertEquals(actual, msg);
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to tap on OK Button after Submission :
	 * 15-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public WeeklyMechanicPage okButtonTappedForAlertSubmission(String msg) throws Exception {
		Boolean iselementpresent = driver.findElements(By.name(WeeklyMechanicLocators.OK)).size()!= 0;
	    if (iselementpresent == true)
	    {
		    System.out.print("Is Present On The Page");
		    WebDriverWait wait = new WebDriverWait(driver,
					WebTimeConstant.WAIT_TIME_SMALL);	
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(WeeklyMechanicLocators.OK)));
		    driver.findElement(By.name(WeeklyMechanicLocators.OK)).click();    
		 }
		 else
		 {
		    System.out.print("is Not Present On The Page");
		 }		 
		return this;		
	}
	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :15-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public WeeklyMechanicPage clickDone() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(WeeklyMechanicLocators.doneButton)));
        driver.findElement(By.name(WeeklyMechanicLocators.doneButton)).click();
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in expense Date : 15-May-2017 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public WeeklyMechanicPage enterValueInWeeklyMechanicFromxpath(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		driver.findElement(By.xpath(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	public WeeklyMechanicPage enterValueInWeeklyMechanicFromxpathEmp(String CrewName, String locators) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));	
		 driver.findElement(By.xpath(locators)).click();
		 driver.findElement(By.xpath(locators)).sendKeys(CrewName);
		 driver.findElement(By.name("Return")).click();
		 return this;
	}
	
}
