package lib.page;

import static org.testng.Assert.assertEquals;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.HideKeyboardStrategy;
import lib.locators.CreateMonthlyBluesheet.MonthlyBluesheetLocators;
import qaframework.custom.WaitForElement;
import qaframework.WebElement.WebTimeConstant;
import qaframework.Configuration.Config_MobileAndWeb;


public class MonthlyBluesheetPage {
	
	WaitForElement wait;
	IOSDriver<MobileElement> driver;
	
	public MonthlyBluesheetPage(IOSDriver<MobileElement> _driver) throws Exception {
		super();
		this.driver = _driver;
		wait = new WaitForElement(this.driver);
	}
	public WebDriver wdriver = Config_MobileAndWeb.wdriver;
	
	/**********************************************************************************
	 * Description : This function select type of monthly bluesheet in create page Date :
	 * 9-May-2017 Author : Ashwini Acharya
	 **********************************************************************************/
	public MonthlyBluesheetPage clickElementByName(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function select type of Fuel in create page
	 *  Date :9-May-2017
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public MonthlyBluesheetPage clickElementByxpath(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in Monthly BlueSheet Report
	 *  Date : 9-May-2017
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public MonthlyBluesheetPage enterValueInMonthlyBluesheet(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		driver.findElement(By.name(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in Fuel
	 *  Date : 9-May-2017 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public MonthlyBluesheetPage enterValueInMonthlyBluesheetFromxpath(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		driver.findElement(By.xpath(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :9-May-2017
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public MonthlyBluesheetPage takePicture() throws Exception {	
		
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MonthlyBluesheetLocators.cameraButton)));
        driver.findElement(By.xpath(MonthlyBluesheetLocators.cameraButton)).click(); 
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("PhotoCapture")));
        driver.findElement(By.name("PhotoCapture")).click();
        driver.findElement(By.name("Use Photo")).click();
       return this;
	}	
	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :9-May-2017
	 * Author : Ashwini Acharya
	 **********************************************************************************/	
	public MonthlyBluesheetPage takePhotos() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		        
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MonthlyBluesheetLocators.photoButton)));
        driver.findElement(By.xpath(MonthlyBluesheetLocators.photoButton)).click();        
        return this;
	}	
	

	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :9-May-2017
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public MonthlyBluesheetPage takePhotosNew() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);		       
         driver.findElement(By.name(MonthlyBluesheetLocators.cameraRoll)).click();
        if (driver.findElement(By.name(MonthlyBluesheetLocators.photoNameiOS11)).isDisplayed()) {
        	    driver.findElement(By.name(MonthlyBluesheetLocators.photoNameiOS11)).click();
        }else{
        	    driver.findElement(By.name(MonthlyBluesheetLocators.photoNameiOS10)).click();
        }        
        return this;
	}	
	
	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :9-May-2017
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public MonthlyBluesheetPage clickSave() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(MonthlyBluesheetLocators.saveButton)));
        driver.findElement(By.name(MonthlyBluesheetLocators.saveButton)).click();
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date in by scrolling
	 * Date : 09-May-2017
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public MonthlyBluesheetPage clickSubmit(String locator) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
        driver.findElement(By.name(locator)).click();
		return this;
	  }	
	
	/**********************************************************************************
	 * Description : This function used to verify alert msg for submitted report :
	 * 09-May-2017 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public MonthlyBluesheetPage verReportIsSubmitted(String msg) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(MonthlyBluesheetLocators.alertForSubmittedReport)));
		driver.findElement(By.name(MonthlyBluesheetLocators.alertForSubmittedReport)).isDisplayed();
		String actual = driver.findElement(By.name(MonthlyBluesheetLocators.alertForSubmittedReport)).getText();
		System.out.println("receive" + actual);
		assertEquals(actual, msg);
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to tap on OK Button after Submission :
	 * 27-Aprill-2017 Author : Ashwini
	 **********************************************************************************/
	public MonthlyBluesheetPage okButtonTappedForAlertSubmission(String msg) throws Exception {
		Boolean iselementpresent = driver.findElements(By.name(MonthlyBluesheetLocators.OK)).size()!= 0;
	    if (iselementpresent == true)
	    {
		    System.out.print("Is Present On The Page");
		    WebDriverWait wait = new WebDriverWait(driver,
					WebTimeConstant.WAIT_TIME_SMALL);	
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(MonthlyBluesheetLocators.OK)));
		    driver.findElement(By.name(MonthlyBluesheetLocators.OK)).click();    
		 }
		 else
		 {
		    System.out.print("is Not Present On The Page");
		 }		 
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to tap on OK Button after Submission :
	 * 27-Aprill-2017 Author : Ashwini
	 **********************************************************************************/
	public MonthlyBluesheetPage okMonthlyBluesheetAlert(String msg) throws Exception {
		Boolean iselementpresent = driver.findElements(By.name(MonthlyBluesheetLocators.alertOkButton)).size()!= 0;
	    if (iselementpresent == true)
	    {
		    System.out.print("Is Present On The Page");
		    WebDriverWait wait = new WebDriverWait(driver,
					WebTimeConstant.WAIT_TIME_SMALL);	
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(MonthlyBluesheetLocators.alertOkButton)));
		    driver.findElement(By.name(MonthlyBluesheetLocators.alertOkButton)).click();    
		 }
		 else
		 {
		    System.out.print("is Not Present On The Page");
		 }		 
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date  by scrolling date
	 * Date : 10-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public MonthlyBluesheetPage scrollTableView(String locator) throws Exception {

    	      driver.findElement(By.xpath(locator)).isDisplayed();    	     
    	      int height = driver.manage().window().getSize().height;
    	      int width = driver.manage().window().getSize().width;    	    
    	      driver.swipe(width, height / 2, 0, height / 2, 250);    	          	      
          return this;
	  }
	
}
