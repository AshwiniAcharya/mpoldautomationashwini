package lib.page;

import static org.testng.Assert.assertEquals;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.HideKeyboardStrategy;

import lib.locators.CreateIncident.IncidentLocators;
import qaframework.custom.WaitForElement;
import qaframework.WebElement.WebTimeConstant;
import qaframework.Configuration.Config_MobileAndWeb;

public class IncidentPage {
	
	WaitForElement wait;
	IOSDriver<MobileElement> driver;
	
	public IncidentPage(IOSDriver<MobileElement> _driver) throws Exception {
		super();
		this.driver = _driver;
		wait = new WaitForElement(this.driver);
	}
	
	public WebDriver wdriver = Config_MobileAndWeb.wdriver;

	/**********************************************************************************
	 * Description : This function used click on the element by the name
	 *  Date : 03-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public IncidentPage clickElementByName(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to click on element using xpath
	 *  Date :03-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public IncidentPage clickElementByxpath(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in Incident
	 *  Date : 03-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public IncidentPage enterValueInIncident(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);		

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		driver.findElement(By.name(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in Incident
	 *  Date : 03-May-2018 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public IncidentPage enterValueInIncidentFromxpath(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		driver.findElement(By.xpath(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :03-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public IncidentPage takePicture() throws Exception {	
		
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(IncidentLocators.cameraButton)));
        driver.findElement(By.xpath(IncidentLocators.cameraButton)).click(); 
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("PhotoCapture")));
        driver.findElement(By.name("PhotoCapture")).click();
        driver.findElement(By.name("Use Photo")).click();
       return this;
	}		

	/**********************************************************************************
	 * Description : This function take a picture using Gallery
	 * Date :03-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/	
	public IncidentPage takePhotos() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		        
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(IncidentLocators.photoButton)));
        driver.findElement(By.xpath(IncidentLocators.photoButton)).click();        
        return this;
	}
	
	/**********************************************************************************
	 * Description : This function taps on Photo button
	 * Date : 30-April-2017
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public IncidentPage takePhotosNew() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);		       
         driver.findElement(By.name(IncidentLocators.cameraRoll)).click();
        if (driver.findElement(By.name(IncidentLocators.photoNameiOS11)).isDisplayed()) {
        	    driver.findElement(By.name(IncidentLocators.photoNameiOS11)).click();
        }else{
        	    driver.findElement(By.name(IncidentLocators.photoNameiOS10)).click();
        }        
        return this;
	}	
	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :03-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public IncidentPage clickSave() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(IncidentLocators.saveButton)));
        driver.findElement(By.name(IncidentLocators.saveButton)).click();
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date in by scrolling
	 * Date : 03-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public IncidentPage clickSubmit(String locator) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
        driver.findElement(By.name(locator)).click();
		return this;

	  }	
		
	/**********************************************************************************
	 * Description : This function used to verify alert msg for submitted report :
	 * 03-May-2018 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public IncidentPage verReportIsSubmitted(String msg) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(IncidentLocators.alertForSubmit)));
		driver.findElement(By.name(IncidentLocators.alertForSubmit)).isDisplayed();
		String actual = driver.findElement(By.name(IncidentLocators.alertForSubmit)).getText();
		System.out.println("receive" + actual);
		assertEquals(actual, msg);
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to tap on OK Button after Submission :
	 * 03-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public IncidentPage okButtonTappedForAlertSubmission(String msg) throws Exception {
		Boolean iselementpresent = driver.findElements(By.name(IncidentLocators.OK)).size()!= 0;
	    if (iselementpresent == true)
	    {
		    System.out.print("Is Present On The Page");
		    WebDriverWait wait = new WebDriverWait(driver,
					WebTimeConstant.WAIT_TIME_SMALL);	
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(IncidentLocators.OK)));
		    driver.findElement(By.name(IncidentLocators.OK)).click();    
		 }
		 else
		 {
		    System.out.print("is Not Present On The Page");
		 }		 
		return this;	
	}
	
	/**********************************************************************************
	 * Description : This function used to click on element using xpath
	 *  Date :04-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public IncidentPage enterSignature(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		
		 
//		 this.element_wait(Locators.signaturePage, "xpath").waitForVisibilityOfElement();
//			driver.findElement(By.xpath(Locators.signaturePage)).isDisplayed();
//			driver.findElement(By.xpath(Locators.signaturePage)).click();
			//WebElement mainMenu = driver.findElement(By.name(locators));
		    TouchAction Action = new TouchAction(driver);
		    Action.press(100,100).moveTo(100,100).release().perform();
		    
			return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date  by scrolling date
	 * Date : 04-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public IncidentPage selecTimeByScrolling(String locator) throws Exception {

//      String startx = driver.findElement(By.name(locator)).getId();
//
//      JavascriptExecutor js = (JavascriptExecutor) driver;
//      HashMap<String, String> scrollObject = new HashMap<String, String>();
//      scrollObject.put("direction", "down");
//      scrollObject.put("element", startx);
//       js.executeScript("mobile: scroll", scrollObject);
       
              
      

    	   	  //this.element_wait(locator, "name").waitForVisibilityOfElement();
    	      driver.findElement(By.xpath(locator)).isDisplayed();
    	      driver.findElement(By.xpath(locator)).click();
    	      //driver.findElement(By.name(Locators.Date)).clear();

    	      String startx = driver.findElement(By.xpath(locator)).getId();

    	//WebElement tableView = (WebElement) driver.findElementsByIosUIAutomation(".XCUIElementTypePickerWheel[2]");
    	//((IOSElement) tableView).scrollTo("22");

    	      JavascriptExecutor js = (JavascriptExecutor) driver;
    	      HashMap<String, String> scrollObject = new HashMap<String, String>();
    	      scrollObject.put("direction", "down");
    	      scrollObject.put("element", startx);
    	       js.executeScript("mobile: scroll", scrollObject);    	       
    	           	      
       
       
       
       
       return this;

	  }
}
