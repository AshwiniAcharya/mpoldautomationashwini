package lib.page;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import lib.locators.CreateWeeklyForeman.WeeklyForemanLocators;
import qaframework.Configuration.Config_MobileAndWeb;
import qaframework.WebElement.WebTimeConstant;
import qaframework.custom.WaitForElement;

public class WeeklyForemanPage {
	WaitForElement wait;
	IOSDriver<MobileElement> driver;
	
	public WeeklyForemanPage(IOSDriver<MobileElement> _driver) throws Exception {
		super();
		this.driver = _driver;
		wait = new WaitForElement(this.driver);
	}
	
    public WebDriver wdriver = Config_MobileAndWeb.wdriver;
	
	/**********************************************************************************
	 * Description : This function used click on the element by the name
	 *  Date : 15-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public WeeklyForemanPage clickElementByName(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date in by scrolling
	 * Date : 29-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public WeeklyForemanPage clickSubmit(String locator) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
        driver.findElement(By.name(locator)).click();
		return this;

	  }	
	
	/**********************************************************************************
	 * Description : This function used to click on element using xpath
	 *  Date :29-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public WeeklyForemanPage clickElementByxpath(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to verify alert msg for submitted report :
	 * 29-May-2018 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public WeeklyForemanPage verReportIsSubmitted(String msg) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(WeeklyForemanLocators.alertForSubmit)));
		driver.findElement(By.name(WeeklyForemanLocators.alertForSubmit)).isDisplayed();
		String actual = driver.findElement(By.name(WeeklyForemanLocators.alertForSubmit)).getText();
		System.out.println("receive" + actual);
		assertEquals(actual, msg);
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to tap on OK Button after Submission :
	 * 29-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public WeeklyForemanPage okButtonTappedForAlertSubmission(String msg) throws Exception {
		Boolean iselementpresent = driver.findElements(By.name(WeeklyForemanLocators.OK)).size()!= 0;
	    if (iselementpresent == true)
	    {
		    System.out.print("Is Present On The Page");
		    WebDriverWait wait = new WebDriverWait(driver,
					WebTimeConstant.WAIT_TIME_SMALL);	
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(WeeklyForemanLocators.OK)));
		    driver.findElement(By.name(WeeklyForemanLocators.OK)).click();    
		 }
		 else
		 {
		    System.out.print("is Not Present On The Page");
		 }		 
		return this;		
	}
	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :15-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public WeeklyForemanPage clickDone() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(WeeklyForemanLocators.doneButton)));
        driver.findElement(By.name(WeeklyForemanLocators.doneButton)).click();
		return this;
	}

}
