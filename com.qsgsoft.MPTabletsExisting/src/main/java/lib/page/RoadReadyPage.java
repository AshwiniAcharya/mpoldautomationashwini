package lib.page;

import static org.testng.Assert.assertEquals;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.HideKeyboardStrategy;
import lib.locators.CreateRoadReady.RoadReadyLocators;
import qaframework.custom.WaitForElement;
import qaframework.WebElement.WebTimeConstant;
import qaframework.Configuration.Config_MobileAndWeb;

public class RoadReadyPage {
	
	WaitForElement wait;
	IOSDriver<MobileElement> driver;
	
	public RoadReadyPage(IOSDriver<MobileElement> _driver) throws Exception {
		super();
		this.driver = _driver;
		wait = new WaitForElement(this.driver);
	}
	
	public WebDriver wdriver = Config_MobileAndWeb.wdriver;
	
	/**********************************************************************************
	 * Description : This function used click on the element by the name
	 *  Date : 07-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public RoadReadyPage clickElementByName(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to click on element using xpath
	 *  Date :07-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public RoadReadyPage clickElementByxpath(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in Incident
	 *  Date : 07-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public RoadReadyPage enterValueInRoadReady(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);		

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		driver.findElement(By.name(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in Incident
	 *  Date : 07-May-2018 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public RoadReadyPage enterValueInRoadReadyFromxpath(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		driver.findElement(By.xpath(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :07-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public RoadReadyPage clickSave() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(RoadReadyLocators.saveButton)));
        driver.findElement(By.name(RoadReadyLocators.saveButton)).click();
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date in by scrolling
	 * Date : 07-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public RoadReadyPage clickSubmit(String locator) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
        driver.findElement(By.name(locator)).click();
		return this;

	  }	
	
	/**********************************************************************************
	 * Description : This function used to click on element using xpath
	 *  Date :07-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public RoadReadyPage enterSignature(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();			
	     TouchAction Action = new TouchAction(driver);
		  Action.press(100,100).moveTo(100,100).release().perform();		    
		  return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date  by scrolling date
	 * Date : 07-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public RoadReadyPage scrollTableView(String locator) throws Exception {

    	      driver.findElement(By.xpath(locator)).isDisplayed();
    	      driver.findElement(By.xpath(locator)).click();
    	      String startx = driver.findElement(By.xpath(locator)).getId();
    	      JavascriptExecutor js = (JavascriptExecutor) driver;
    	      HashMap<String, String> scrollObject = new HashMap<String, String>();
    	      scrollObject.put("direction", "down");
    	      scrollObject.put("element", startx);
    	       js.executeScript("mobile: scroll", scrollObject);       
           return this;
	  }
	
	/**********************************************************************************
	 * Description : This function used to verify alert msg for submitted report :
	 * 07-May-2018 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public RoadReadyPage verReportIsSubmitted(String msg) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(RoadReadyLocators.alertForSubmit)));
		driver.findElement(By.name(RoadReadyLocators.alertForSubmit)).isDisplayed();
		String actual = driver.findElement(By.name(RoadReadyLocators.alertForSubmit)).getText();
		System.out.println("receive" + actual);
		assertEquals(actual, msg);
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to tap on OK Button after Submission :
	 * 07-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public RoadReadyPage okButtonTappedForAlertSubmission(String msg) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(RoadReadyLocators.OK)));
		driver.findElement(By.name(RoadReadyLocators.OK)).click();
		return this;		
	}

}
