package lib.page;

import static org.testng.Assert.assertEquals;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.HideKeyboardStrategy;
import lib.locators.CreateDailyTimesheet.DailyTimesheetLocators;
import lib.locators.CreateWeeklyMechanic.WeeklyMechanicLocators;
import qaframework.Configuration.Config_MobileAndWeb;
import qaframework.WebElement.WebTimeConstant;
import qaframework.custom.WaitForElement;

public class DailyTimesheetPage {
	WaitForElement wait;
	IOSDriver<MobileElement> driver;
	
	public DailyTimesheetPage(IOSDriver<MobileElement> _driver) throws Exception {
		super();
		this.driver = _driver;
		wait = new WaitForElement(this.driver);
	}
	
	public WebDriver wdriver = Config_MobileAndWeb.wdriver;
	
	/**********************************************************************************
	 * Description : This function used click on the element by the name
	 *  Date : 28-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public DailyTimesheetPage clickElementByName(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to click on element using xpath
	 *  Date :28-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public DailyTimesheetPage clickElementByxpath(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in Daily Timesheet
	 *  Date : 28-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public DailyTimesheetPage enterValueInDailyTimesheet(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);		

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		driver.findElement(By.name(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date in by scrolling
	 * Date : 28-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public DailyTimesheetPage clickSubmit(String locator) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
        driver.findElement(By.name(locator)).click();
		return this;

	  }	

	/**********************************************************************************
	 * Description : This function used to verify alert msg for submitted report :
	 * 28-May-2018 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public DailyTimesheetPage verReportIsSubmitted(String msg) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(WeeklyMechanicLocators.alertForSubmit)));
		driver.findElement(By.name(DailyTimesheetLocators.alertForSubmit)).isDisplayed();
		String actual = driver.findElement(By.name(DailyTimesheetLocators.alertForSubmit)).getText();
		System.out.println("receive" + actual);
		assertEquals(actual, msg);
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to tap on OK Button after Submission :
	 * 28-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public DailyTimesheetPage okButtonTappedForAlertSubmission(String msg) throws Exception {
		Boolean iselementpresent = driver.findElements(By.name(DailyTimesheetLocators.OK)).size()!= 0;
	    if (iselementpresent == true)
	    {
		    System.out.print("Is Present On The Page");
		    WebDriverWait wait = new WebDriverWait(driver,
					WebTimeConstant.WAIT_TIME_SMALL);	
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(DailyTimesheetLocators.OK)));
		    driver.findElement(By.name(DailyTimesheetLocators.OK)).click();    
		 }
		 else
		 {
		    System.out.print("is Not Present On The Page");
		 }		 
		return this;		
	}
	
	/**********************************************************************************
	 * Description : This function used to tap on OK Button after Submission :
	 * 28-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public DailyTimesheetPage yesTappedForAlert(String msg) throws Exception {
		Boolean iselementpresent = driver.findElements(By.name(DailyTimesheetLocators.Yes)).size()!= 0;
	    if (iselementpresent == true)
	    {
		    System.out.print("Is Present On The Page");
		    WebDriverWait wait = new WebDriverWait(driver,
					WebTimeConstant.WAIT_TIME_SMALL);	
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(DailyTimesheetLocators.Yes)));
		    driver.findElement(By.name(DailyTimesheetLocators.Yes)).click();    
		 }
		 else
		 {
		    System.out.print("is Not Present On The Page");
		 }		 
		return this;		
	}
	
	/**********************************************************************************
	 * Description : This function enter value in expense Date : 28-May-2017 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public DailyTimesheetPage enterValueInDailyTimesheetFromxpath(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		driver.findElement(By.xpath(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	public DailyTimesheetPage enterValueInDailyTimesheetFromxpathEmp(String CrewName, String locators) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));	
		 driver.findElement(By.xpath(locators)).click();
		 driver.findElement(By.xpath(locators)).sendKeys(CrewName);
		 driver.findElement(By.name("Return")).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date  by scrolling date
	 * Date : 04-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public DailyTimesheetPage selecTimeByScrolling(String locator) throws Exception {
		
    	     driver.findElement(By.xpath(locator)).isDisplayed();
    	     driver.findElement(By.xpath(locator)).click();    	    
    	     String startx = driver.findElement(By.xpath(locator)).getId();
    	     JavascriptExecutor js = (JavascriptExecutor) driver;
    	     HashMap<String, String> scrollObject = new HashMap<String, String>();
    	     scrollObject.put("direction", "down");
    	     scrollObject.put("element", startx);
    	     js.executeScript("mobile: scroll", scrollObject);   
         return this;

	  }
	
	/**********************************************************************************
	 * Description : This function used to select className 
	 * Date : 30-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public DailyTimesheetPage clickElementByxpathClass(String locators) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
		driver.findElement(By.xpath("//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[4]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeCollectionView[1]/XCUIElementTypeCell[12]")).click();
	    return this;
	}
}
