package lib.page;

import static org.testng.Assert.assertEquals;

import java.awt.Desktop.Action;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.HideKeyboardStrategy;
import lib.locators.CreateAtvSafety.AtvSafetyLocators;
import qaframework.Configuration.Config_MobileAndWeb;
import qaframework.WebElement.WebTimeConstant;
import qaframework.custom.WaitForElement;

public class AtvSafetyPage {
	WaitForElement wait;
	IOSDriver<MobileElement> driver;
	
	public AtvSafetyPage(IOSDriver<MobileElement> _driver) throws Exception {
		super();
		this.driver = _driver;
		wait = new WaitForElement(this.driver);
	}
	
	 public WebDriver wdriver = Config_MobileAndWeb.wdriver;
		
		/**********************************************************************************
		 * Description : This function used click on the element by the name
		 *  Date : 11-May-2018
		 *  Author : Ashwini Acharya
		 **********************************************************************************/
	public AtvSafetyPage clickElementByName(String locators) throws Exception {
			WebDriverWait wait = new WebDriverWait(driver,
					WebTimeConstant.WAIT_TIME_SMALL);
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
	         driver.findElement(By.name(locators)).click();
			 return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in Incident
	 *  Date : 11-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public AtvSafetyPage enterValueInAtvSafety(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);	
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		driver.findElement(By.name(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date in by scrolling
	 * Date : 11-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public AtvSafetyPage clickSubmit(String locator) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
        driver.findElement(By.name(locator)).click();
		return this;
	  }	
	
	/**********************************************************************************
	 * Description : This function used to verify alert msg for submitted report :
	 * 08-May-2018 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public AtvSafetyPage verReportIsSubmitted(String msg) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(AtvSafetyLocators.alertForSubmittedReport)));
		driver.findElement(By.name(AtvSafetyLocators.alertForSubmittedReport)).isDisplayed();
		String actual = driver.findElement(By.name(AtvSafetyLocators.alertForSubmittedReport)).getText();
		System.out.println("receive" + actual);
		assertEquals(actual, msg);
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to tap on OK Button after Submission :
	 * 11-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public AtvSafetyPage okButtonTappedForAlertSubmission(String msg) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
		WebTimeConstant.WAIT_TIME_SMALL);	
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(AtvSafetyLocators.OK)));
		driver.findElement(By.name(AtvSafetyLocators.OK)).click();   
		return this;		
	}
	
	/**********************************************************************************
	 * Description : This function used to tap on OK Button after Submission :
	 * 11-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public AtvSafetyPage doneTap(String msg) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
		WebTimeConstant.WAIT_TIME_SMALL);	
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(AtvSafetyLocators.done)));
		driver.findElement(By.name(AtvSafetyLocators.done)).click();   
		return this;		
	}

	/**********************************************************************************
	 * Description : This function used to click on element using xpath
	 *  Date :11-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public AtvSafetyPage enterSignature(String locators) throws Exception {
		
		 String startx = driver.findElement(By.name(locators)).getId();		
	     JavascriptExecutor js = (JavascriptExecutor) driver;
	     HashMap<String, Object> params = new HashMap<String, Object>();
		 params.put("duration", 1.0);
		 params.put("fromX", 100);
		 params.put("fromY", 100);
		 params.put("toX", 200);
		 params.put("toY", 200);
		 params.put("element", startx);
		 js.executeScript("mobile: dragFromToForDuration", params);		
		 return this;
	}
	
	

	/**********************************************************************************
	 * Description : This function select date  by scrolling date
	 * Date : 11-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public AtvSafetyPage scrollTableView(String locator) throws Exception {
    	      driver.findElement(By.xpath(locator)).isDisplayed();    	     
    	      int height = driver.manage().window().getSize().height;
    	      int width = driver.manage().window().getSize().width;    	    
    	      driver.swipe(width, height / 2, 0, height / 2, 250);    	          	      
          return this;
	  }
}
