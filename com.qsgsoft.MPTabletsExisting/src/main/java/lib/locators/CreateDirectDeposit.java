package lib.locators;

public class CreateDirectDeposit {
	public static class DirectDepositLocators {
		public static final String directDepositIcon = "directIcn",
				                                 addIcon = "plus 1",
				                                 employeeName = "empName",
				                                 submitButton = "submit",
				                                 ssn = "ssn",
				                                 address = "address",
				                                 city = "city",
				                                 zip = "zip",
				                                 phone = "phone",
				                                 bankName = "bank",
				                                 accountName = "account",
				                                 route = "route",
				                                 phoneNumber = "phoneNumber",
				                                 bankAddress = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTextField[12]",
				                                 supervisorSig = "supervisorSig",
				                                 attachButton = "attach",
				                                 tickButton = "tickButton",
				                                 photoButton = "photoButton",
				                                 cameraButton = "cameraButton",
				                                 scrollView = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]",
				                                 stateCell = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[6]",
				                                 doneButton = "Done",
				                                 state = "state",
				                                 saveButton = "Save",
				                                 OK ="OK",
				                                 alertForSubmittedReport = "Your Direct Deposit Form is Submitted",	
	                                             Allow = "Allow",
	                                             cameraRoll = "Automation",		                                          
	                                             photoNameiOS11 = "Photo, Portrait, 25 May, 1:18 PM",
                                                 photoNameiOS10 =	"Photo, Portrait, 23 April, 5:33 PM";
		}

}
