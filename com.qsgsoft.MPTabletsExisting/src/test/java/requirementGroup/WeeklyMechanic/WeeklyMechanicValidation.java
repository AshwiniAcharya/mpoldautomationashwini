package requirementGroup.WeeklyMechanic;

import org.testng.annotations.Test;

import lib.dataObject.LoginData;
import lib.dataObject.ProjectSiteData;
import lib.dataObject.WeeklyMechanicData;
import lib.locators.CreateProjectSite.ProjectSiteLocators;
import lib.locators.CreateWeeklyMechanic.WeeklyMechanicLocators;
import lib.page.HomePage;
import lib.page.LoginPage;
import lib.page.ProjectSitePage;
import lib.page.WeeklyMechanicPage;
import lib.page.YopMailPage;
import qaframework.Configuration.Config_MobileAndWeb;

public class WeeklyMechanicValidation extends Config_MobileAndWeb  {
	public void login() throws Exception {
		LoginPage objLogin = new LoginPage(IOS_Driver);
		HomePage objHome = new HomePage(IOS_Driver);
		LoginData objLoginData = new LoginData();
		WeeklyMechanicPage weeklyMechanic = new WeeklyMechanicPage(IOS_Driver);
		objLogin.enterPhoneNumber(objLoginData.ForemanLoggedInNumber).clickSignIn();
		objHome.clickWeeklyMechanic();			
	}
	
	 public void createWeeklyMechanic() throws Exception {
			
		    WeeklyMechanicPage weeklyMechanic = new WeeklyMechanicPage(IOS_Driver);
		    WeeklyMechanicData objWeeklyMechanic = new WeeklyMechanicData();		    
		    weeklyMechanic.clickElementByName(WeeklyMechanicLocators.weeklySegment);	
		    weeklyMechanic.clickElementByName(WeeklyMechanicLocators.addButton);
		    weeklyMechanic.clickElementByName(WeeklyMechanicLocators.createWeekly);			    
		    weeklyMechanic.clickElementByxpath(WeeklyMechanicLocators.startDate);	
		    weeklyMechanic.clickElementByxpath(WeeklyMechanicLocators.endDate);		
		    weeklyMechanic.clickElementByName(WeeklyMechanicLocators.create);		    
		    weeklyMechanic.enterValueInWeeklyMechanic(objWeeklyMechanic.company, WeeklyMechanicLocators.company);
		    weeklyMechanic.clickElementByName(WeeklyMechanicLocators.monday);
		    weeklyMechanic.enterValueInWeeklyMechanic(objWeeklyMechanic.remarks, WeeklyMechanicLocators.remarks);
		    weeklyMechanic.clickElementByName(WeeklyMechanicLocators.crewMembers);
		    weeklyMechanic.enterValueInWeeklyMechanicFromxpathEmp(objWeeklyMechanic.empName, WeeklyMechanicLocators.empName);		    
		    weeklyMechanic.clickElementByxpathClass(WeeklyMechanicLocators.className);
		    weeklyMechanic.clickElementByxpath(WeeklyMechanicLocators.classNameFirstCell);
		    weeklyMechanic.clickElementByxpath(WeeklyMechanicLocators.st);
		    weeklyMechanic.enterValueInWeeklyMechanic(objWeeklyMechanic.stHours, WeeklyMechanicLocators.stHours);
		    weeklyMechanic.enterValueInWeeklyMechanic(objWeeklyMechanic.stjob, WeeklyMechanicLocators.stjob);
		    weeklyMechanic.enterValueInWeeklyMechanic(objWeeklyMechanic.stwo, WeeklyMechanicLocators.stwo);
		    weeklyMechanic.clickElementByName(WeeklyMechanicLocators.stugoh);	
		    weeklyMechanic.clickElementByxpath(WeeklyMechanicLocators.ugOhCell);	
		    weeklyMechanic.clickElementByName(WeeklyMechanicLocators.crossButton);			    
		    weeklyMechanic.clickElementByxpath(WeeklyMechanicLocators.ot);
		    weeklyMechanic.enterValueInWeeklyMechanicFromxpath(objWeeklyMechanic.otHours, WeeklyMechanicLocators.otHours);
//		    weeklyMechanic.enterValueInWeeklyMechanicFromxpath(objWeeklyMechanic.otJob, WeeklyMechanicLocators.otJob);
		    //weeklyMechanic.enterValueInWeeklyMechanicFromxpath(objWeeklyMechanic.otwo, WeeklyMechanicLocators.otwo);
		    weeklyMechanic.clickElementByName(WeeklyMechanicLocators.crossButton);		
		    weeklyMechanic.clickElementByxpath(WeeklyMechanicLocators.dt);
		    weeklyMechanic.enterValueInWeeklyMechanicFromxpath(objWeeklyMechanic.dtHours, WeeklyMechanicLocators.dtHours);
		    weeklyMechanic.clickElementByName(WeeklyMechanicLocators.crossButton);		
		    weeklyMechanic.enterValueInWeeklyMechanicFromxpath(objWeeklyMechanic.perDiem, WeeklyMechanicLocators.perDiem);
		   	weeklyMechanic.enterValueInWeeklyMechanicFromxpathEmp(objWeeklyMechanic.secondEmpName, WeeklyMechanicLocators.secondEmpName);
		    weeklyMechanic.clickElementByxpath(WeeklyMechanicLocators.secondClassName);		    
		    weeklyMechanic.clickElementByxpath(WeeklyMechanicLocators.classNameSecondCell);
		    
		    weeklyMechanic.clickElementByxpath(WeeklyMechanicLocators.secondST);
		    weeklyMechanic.enterValueInWeeklyMechanicFromxpath(objWeeklyMechanic.secondStHours, WeeklyMechanicLocators.secondStHours);
		    weeklyMechanic.clickElementByName(WeeklyMechanicLocators.crossButton);	
		    
		    weeklyMechanic.clickElementByxpath(WeeklyMechanicLocators.secondOT);
		    weeklyMechanic.enterValueInWeeklyMechanicFromxpath(objWeeklyMechanic.secondOtHours, WeeklyMechanicLocators.secondOtHours);
		    weeklyMechanic.clickElementByName(WeeklyMechanicLocators.crossButton);	
		    
		    weeklyMechanic.clickElementByxpath(WeeklyMechanicLocators.secondDT);
		    weeklyMechanic.enterValueInWeeklyMechanicFromxpath(objWeeklyMechanic.secondDtHours, WeeklyMechanicLocators.secondDtHours);
		    weeklyMechanic.clickElementByName(WeeklyMechanicLocators.crossButton);			    
		    		  
			weeklyMechanic.enterValueInWeeklyMechanicFromxpath(objWeeklyMechanic.perDiemSecond, WeeklyMechanicLocators.perDiemSecond);
			weeklyMechanic.clickElementByName(WeeklyMechanicLocators.equipment);
			weeklyMechanic.clickElementByxpath(WeeklyMechanicLocators.equipFirstName);
			weeklyMechanic.clickElementByxpath(WeeklyMechanicLocators.equipmentFirstCell);
			weeklyMechanic.enterValueInWeeklyMechanicFromxpath(objWeeklyMechanic.monEquipment, WeeklyMechanicLocators.monEquipment);
			weeklyMechanic.clickElementByxpath(WeeklyMechanicLocators.equipSecondName);
			weeklyMechanic.clickElementByxpath(WeeklyMechanicLocators.equipmentSecondCell);
			weeklyMechanic.clickElementByName(WeeklyMechanicLocators.doneButton);		    
	}	 
	 
		
		/**********************************************************************************************************
		 * 'Description: Verify that able to create the report with attachement and submit
		 *  'Date: 16-May-2018
		 * 'Author: Ashwini Acharya
		 **********************************************************************************************************/
		
		@Test(priority = 1, description = "Verify that able to create the report with attachement and submit and receive the email")
	    public void RTS1() throws Exception {

			TCID = "RTS1";
			strTO = "Verify that able to create the report with attachement and submit and receive the email";
			WeeklyMechanicPage weeklyMechanic = new WeeklyMechanicPage(IOS_Driver);
			WeeklyMechanicData objweeklyMechanicData = new WeeklyMechanicData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			login();		
			createWeeklyMechanic();			
			weeklyMechanic.clickSubmit(WeeklyMechanicLocators.submitButton);	
			weeklyMechanic.clickElementByName(WeeklyMechanicLocators.doneSubmit);	
			weeklyMechanic.verReportIsSubmitted(objweeklyMechanicData.alertForSubmit);
			weeklyMechanic.okButtonTappedForAlertSubmission(objweeklyMechanicData.OK);
		    // launch yopmail
		    objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
		    gstrResult = "PASS";
			
		}

}
