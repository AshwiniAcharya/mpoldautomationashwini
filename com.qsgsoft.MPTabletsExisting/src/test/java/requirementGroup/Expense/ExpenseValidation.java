package requirementGroup.Expense;


import org.testng.annotations.Test;

import lib.dataObject.ExpenseData;
import lib.dataObject.LoginData;
import lib.page.ExpensePage;
import lib.page.HomePage;
import lib.page.LoginPage;
import lib.page.YopMailPage;
import lib.locators.CreateExpense;
import lib.locators.CreateExpense.Locators;
import lib.locators.Yopmail;
import qaframework.Configuration.Config_MobileAndWeb;

public class ExpenseValidation extends Config_MobileAndWeb{		

	public void login() throws Exception {
		LoginPage objLogin = new LoginPage(IOS_Driver);
		HomePage objHome = new HomePage(IOS_Driver);
		LoginData objLoginData = new LoginData();
		ExpensePage expense = new ExpensePage(IOS_Driver);
		
		//objLogin.tap7times();
		objLogin.enterPhoneNumber(objLoginData.MechanicLoggedInNumber).clickSignIn();
		objHome.allowButtonTapped(Locators.Allow);
		objHome.okButtonTappedForAlert(Locators.OK);	
		objHome.okButtonTappedForAlert(Locators.OK);		
		objHome.clickExpense();
		expense.clickElementByName(Locators.addIcon);		
	}
	
	public void createExpense() throws Exception {
		
		ExpensePage expense = new ExpensePage(IOS_Driver);
		ExpenseData objExpenseData = new ExpenseData();
		expense.okButtonTappedForAlertSubmission(objExpenseData.OK);
		expense.clickElementByxpath(Locators.typeOfExpense);
		expense.clickElementByxpath(Locators.typeOfExpenseDropDown);
		expense.clickElementByName(Locators.receiptDate);
		//expense.selectDateByScrollingDate(Locators.date);
		expense.enterValueInExpense(objExpenseData.EmployeeName, Locators.employeeName);
		expense.enterValueInExpense(objExpenseData.JobNumber, Locators.jobNumber);
		expense.enterValueInExpense(objExpenseData.VehicleNumber, Locators.vehicleNumber);
		expense.enterValueInExpenseFromxpath(objExpenseData.TotalAmount, Locators.totalAmount);
		expense.enterValueInExpenseFromxpath(objExpenseData.Description, Locators.description);
		expense.takePhotos();	 		
		expense.takePhotosNew(); 
		expense.clickSave();
	}
	
     public void createExpenseSecond() throws Exception {
		
		ExpensePage expense = new ExpensePage(IOS_Driver);
		ExpenseData objExpenseData = new ExpenseData();
		
		expense.clickElementByxpath(Locators.typeOfExpense);
		expense.clickElementByxpath(Locators.typeOfExpenseDropDown);
		expense.clickElementByName(Locators.receiptDate);
		//expense.selectDateByScrollingDate(Locators.date);
		expense.enterValueInExpense(objExpenseData.EmployeeNameSecond, Locators.employeeName);
		expense.enterValueInExpense(objExpenseData.jobNumbersecond, Locators.jobNumber);
		expense.enterValueInExpense(objExpenseData.VehicleNumber, Locators.vehicleNumber);
		expense.enterValueInExpenseFromxpath(objExpenseData.TotalAmount, Locators.totalAmount);
		expense.enterValueInExpenseFromxpath(objExpenseData.Description, Locators.description);
		//expense.takePhotos();	 
		//expense.okButtonTappedForAlertSubmission(objExpenseData.OK);
		//expense.takePhotosNew();
		expense.takePicture();
		expense.clickSave();
	}
	
	/**********************************************************************************************************
	 * 'Description: Verify that able to create the report with attachement and submit
	 *  'Date: 24-4-2018
	 * 'Author: chaithra0
	 **********************************************************************************************************/
	
	@Test(priority = 1, description = "Verify that able to create the report with attachement and submit")
    public void RTS1() throws Exception {

		TCID = "RTS1";
		strTO = "Verify that able to create the report with attachement and submit";
		ExpensePage expense = new ExpensePage(IOS_Driver);
		ExpenseData objExpenseData = new ExpenseData();
		login();
		createExpense();
		expense.clickSubmit(Locators.submit);
		expense.verReportIsSubmitted(objExpenseData.AlertForSubmit);
		expense.okButtonTappedForAlertSubmission(objExpenseData.OK);
	}
	
	/**********************************************************************************************************
	 * 'Description: Verify that able to create the report with attachement and submit
	 *  'Date: 24-4-2018
	 * 'Author: chaithra
	 **********************************************************************************************************/
	
	@Test(priority = 2, description = "Verify that able to create the report with attachement and submit")
    public void RTS2() throws Exception {

		TCID = "RTS2";
		strTO = "Verify that able to create the report with attachement and submit";
		ExpensePage expense = new ExpensePage(IOS_Driver);
		ExpenseData objExpenseData = new ExpenseData();
		YopMailPage objYopMail = new YopMailPage(this.wdriver);
		expense.clickElementByName(Locators.addIcon);
		createExpenseSecond();
		expense.clickSubmit(Locators.submit);
		expense.verReportIsSubmitted(objExpenseData.AlertForSubmit);
		expense.okButtonTappedForAlertSubmission(objExpenseData.OK);
		
//		String options[] = {"00118", "5003", "65412",
//		"Fuel"};

	// launch yopmail
	objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
	gstrResult = "PASS";
	
		
	}
	
	
	



}
