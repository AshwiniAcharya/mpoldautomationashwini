package requirementGroup.DailyTimesheet;

import org.testng.annotations.Test;

import lib.dataObject.DailyTimesheetData;
import lib.dataObject.LoginData;
import lib.dataObject.WeeklyMechanicData;
import lib.locators.CreateDailyTimesheet.DailyTimesheetLocators;
import lib.locators.CreateWeeklyMechanic.WeeklyMechanicLocators;
import lib.page.DailyTimesheetPage;
import lib.page.HomePage;
import lib.page.LoginPage;
import lib.page.WeeklyMechanicPage;
import lib.page.YopMailPage;
import qaframework.Configuration.Config_MobileAndWeb;

public class DailyTimesheetValidation extends Config_MobileAndWeb {
	
	public void login() throws Exception {
		LoginPage objLogin = new LoginPage(IOS_Driver);
		HomePage objHome = new HomePage(IOS_Driver);
		LoginData objLoginData = new LoginData();
		DailyTimesheetPage dailyTimesheet = new DailyTimesheetPage(IOS_Driver);
		objLogin.enterPhoneNumber(objLoginData.ForemanLoggedInNumber).clickSignIn();
		objHome.clickWeeklyMechanic();			
	}
	
	 public void createDailyTimesheetNW() throws Exception {
			
		    DailyTimesheetPage dailyTimesheet = new DailyTimesheetPage(IOS_Driver);
		    DailyTimesheetData objdailyTimesheet = new  DailyTimesheetData();	   
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.addButton);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.timesheetTemplate);	
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.northWestern);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.date);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.startTime);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.doneToolBar);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.endTime);
		    dailyTimesheet.selecTimeByScrolling(DailyTimesheetLocators.scrollTime);	
		    dailyTimesheet.enterValueInDailyTimesheet(objdailyTimesheet.jobNumber, DailyTimesheetLocators.jobNumber);
		    dailyTimesheet.enterValueInDailyTimesheet(objdailyTimesheet.remarksNW, DailyTimesheetLocators.remarks);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.hotlineHold);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.crewMembers);
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.workOrderfirstCol);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.woNW1, DailyTimesheetLocators.woText);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.woDone);	
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.workOrderSecCol);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.woNW2, DailyTimesheetLocators.woText);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.woDone);	
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.jobLocationFirstCol);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.jobLocationNW1, DailyTimesheetLocators.jobLocationText);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.woDone);	
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.jobLocationSecCol);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.jobLocationNW2, DailyTimesheetLocators.jobLocationText);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.woDone);	
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.opFirstCol);
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.opFirstCell);		    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.opSecCol);
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.opSecCell);		    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.ugOhFirstCol);
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.ugCell);		    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.ugOhSecCol);
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.ohCell);		    
		    dailyTimesheet.enterValueInDailyTimesheetFromxpathEmp(objdailyTimesheet.empNameNW1, DailyTimesheetLocators.empNamefirst);	    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.classNamefirst);
		    dailyTimesheet.clickElementByName("OP");		    
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.stNW1, DailyTimesheetLocators.stFirst);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.otNW1, DailyTimesheetLocators.otFirst);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.dtNW1, DailyTimesheetLocators.dtFirst);		    
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.perdiemNW1, DailyTimesheetLocators.perDiem);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpathEmp(objdailyTimesheet.empNameNW2, DailyTimesheetLocators.empNameSec);		    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.classNameSec);
		    dailyTimesheet.clickElementByName("AB");		    
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.stNW2, DailyTimesheetLocators.stSec);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.otNW2, DailyTimesheetLocators.otSec);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.dtNW2, DailyTimesheetLocators.dtSec);		    
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.perdiemNW2, DailyTimesheetLocators.perDiemSec);		    
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.equipment);				    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.equipmentName);
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.equipFirstCell);		    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.equip2NW);
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.equipSecCell);		    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.equip3NW);
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.equipThirdCell);		    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.equip4NW);
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.equipFourthCell);		   
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.doneMain);		
	}	 
	 
	 public void createDailyTimesheetMP() throws Exception{
		 
		    DailyTimesheetPage dailyTimesheet = new DailyTimesheetPage(IOS_Driver);
		    DailyTimesheetData objdailyTimesheet = new  DailyTimesheetData();	   
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.addButton);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.timesheetTemplate);	
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.mountainPower);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.date);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.startTime);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.doneToolBar);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.endTime);
		    dailyTimesheet.selecTimeByScrolling(DailyTimesheetLocators.scrollTime);	
		    dailyTimesheet.enterValueInDailyTimesheet(objdailyTimesheet.jobNumber, DailyTimesheetLocators.jobNumber);
		    dailyTimesheet.enterValueInDailyTimesheet(objdailyTimesheet.remarksMP, DailyTimesheetLocators.remarks);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.hotlineHold);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.crewMembers);
		    dailyTimesheet.yesTappedForAlert(objdailyTimesheet.Yes);		    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.workOrderfirstCol);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.woMP1, DailyTimesheetLocators.woText);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.woDone);	
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.workOrderSecCol);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.woMP2, DailyTimesheetLocators.woText);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.woDone);	
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.jobLocationFirstCol);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.jobLocationMP1, DailyTimesheetLocators.jobLocationText);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.woDone);	
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.jobLocationSecCol);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.jobLocationMP2, DailyTimesheetLocators.jobLocationText);
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.woDone);			       
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.ugOhMPFirstCol);		    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.ugCell);			    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.ugOhMPSecCol);
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.ohCell);			    
		    dailyTimesheet.enterValueInDailyTimesheetFromxpathEmp(objdailyTimesheet.empNameMP, DailyTimesheetLocators.empFirstMP);	    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.classNameMPFirst);
		    dailyTimesheet.clickElementByName("Z");		    
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.stMP1, DailyTimesheetLocators.stMPFirst);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.otMP1, DailyTimesheetLocators.otMPFirst);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.dtMP1, DailyTimesheetLocators.dtMPFirst);		    
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.perdiemMP1, DailyTimesheetLocators.perDiemMP);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpathEmp(objdailyTimesheet.empNameMP2, DailyTimesheetLocators.empNameSecMP);		    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.classNameSecMP);
		    dailyTimesheet.clickElementByName("V");		    
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.stMP2, DailyTimesheetLocators.stMPSec);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.otMP2, DailyTimesheetLocators.otMPSec);
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.dtMP2, DailyTimesheetLocators.dtMPSec);		    
		    dailyTimesheet.enterValueInDailyTimesheetFromxpath(objdailyTimesheet.perdiemMP2, DailyTimesheetLocators.perDiemSecMP);		    
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.equipment);				    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.equipMPFirst);
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.equipFirstCell);		    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.equipMPSec);
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.equipSecCell);		    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.equipMPThird);
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.equipThirdCell);		    
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.equipMPFourth);
		    dailyTimesheet.clickElementByxpath(DailyTimesheetLocators.equipFourthCell);		   
		    dailyTimesheet.clickElementByName(DailyTimesheetLocators.doneMain);		
		 
	 }
	 
	 /**********************************************************************************************************
		 * 'Description: Verify that able to create the report  and submit
		 *  'Date: 16-May-2018
		 * 'Author: Ashwini Acharya
		 **********************************************************************************************************/
		
		@Test(priority = 1, description = "Verify that able to create the report with attachement and submit and receive the email")
	    public void RTS1() throws Exception {

			TCID = "RTS1";
			strTO = "Verify that able to create the report with attachement and submit and receive the email";
			DailyTimesheetPage dailyTimesheet = new DailyTimesheetPage(IOS_Driver);
			DailyTimesheetData objDailyTimesheetData = new DailyTimesheetData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			login();		
			createDailyTimesheetNW();			
			dailyTimesheet.clickSubmit(DailyTimesheetLocators.inspect);	
			dailyTimesheet.clickElementByName(DailyTimesheetLocators.doneComplete);	
			dailyTimesheet.clickElementByName(DailyTimesheetLocators.doneComplete);	
			dailyTimesheet.verReportIsSubmitted(objDailyTimesheetData.alertForSubmit);
			dailyTimesheet.okButtonTappedForAlertSubmission(objDailyTimesheetData.OK);
		    // launch yopmail
		    objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
		    gstrResult = "PASS";
			
		}
		
		/**********************************************************************************************************
		 * 'Description: Verify that able to create the report  and submit
		 *  'Date: 16-May-2018
		 * 'Author: Ashwini Acharya
		 **********************************************************************************************************/
		
		@Test(priority = 2, description = "Verify that able to create the report with attachement and submit and receive the email")
	    public void RTS2() throws Exception {
			TCID = "RTS2";
			strTO = "Verify that able to create the report with attachement and submit and receive the email";
			DailyTimesheetPage dailyTimesheet = new DailyTimesheetPage(IOS_Driver);
			DailyTimesheetData objDailyTimesheetData = new DailyTimesheetData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);			
			createDailyTimesheetMP();			
			dailyTimesheet.clickSubmit(DailyTimesheetLocators.inspect);	
			dailyTimesheet.clickElementByName(DailyTimesheetLocators.doneComplete);	
			dailyTimesheet.clickElementByName(DailyTimesheetLocators.doneComplete);	
			dailyTimesheet.verReportIsSubmitted(objDailyTimesheetData.alertForSubmit);
			dailyTimesheet.okButtonTappedForAlertSubmission(objDailyTimesheetData.OK);
		    // launch yopmail
		    objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
		    gstrResult = "PASS";			
		}
}
