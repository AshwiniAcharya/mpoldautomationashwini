package requirementGroup.WeeklyForeman;

import org.testng.annotations.Test;

import lib.dataObject.LoginData;
import lib.dataObject.WeeklyForemanData;
import lib.dataObject.WeeklyMechanicData;
import lib.locators.CreateWeeklyForeman.WeeklyForemanLocators;
import lib.locators.CreateWeeklyMechanic.WeeklyMechanicLocators;
import lib.page.HomePage;
import lib.page.LoginPage;
import lib.page.WeeklyForemanPage;
import lib.page.WeeklyMechanicPage;
import lib.page.YopMailPage;
import qaframework.Configuration.Config_MobileAndWeb;

public class WeeklyForemanValidation extends Config_MobileAndWeb {
	public void login() throws Exception {
		LoginPage objLogin = new LoginPage(IOS_Driver);
		HomePage objHome = new HomePage(IOS_Driver);
		LoginData objLoginData = new LoginData();
		WeeklyForemanPage weeklyForeman = new WeeklyForemanPage(IOS_Driver);
		objLogin.enterPhoneNumber(objLoginData.ForemanLoggedInNumber).clickSignIn();
		objHome.clickWeeklyMechanic();			
	}
	
	 public void createWeeklyForeman() throws Exception {
			
		 WeeklyForemanPage weeklyForeman = new WeeklyForemanPage(IOS_Driver);
		 WeeklyForemanData objWeeklyForeman = new WeeklyForemanData();		    
		 weeklyForeman.clickElementByName(WeeklyForemanLocators.weeklySegment);	
		 weeklyForeman.clickElementByName(WeeklyForemanLocators.addButton);
		 weeklyForeman.clickElementByName(WeeklyForemanLocators.createWeekly);			    
		 weeklyForeman.clickElementByxpath(WeeklyForemanLocators.startDate);	
		 weeklyForeman.clickElementByxpath(WeeklyForemanLocators.endDate);		
		 weeklyForeman.clickElementByName(WeeklyForemanLocators.generate);			   
		 weeklyForeman.clickElementByName(WeeklyForemanLocators.crewMembers);		   
		 weeklyForeman.clickElementByName(WeeklyForemanLocators.equipment);		
		 weeklyForeman.clickElementByName(WeeklyForemanLocators.doneButton);		    
	}	 

	 /**********************************************************************************************************
		 * 'Description: Verify that able to create the report with attachement and submit
		 *  'Date: 16-May-2018
		 * 'Author: Ashwini Acharya
		 **********************************************************************************************************/
		
		@Test(priority = 1, description = "Verify that able to create the report with attachement and submit and receive the email")
	    public void RTS1() throws Exception {

			TCID = "RTS1";
			strTO = "Verify that able to create the report with attachement and submit and receive the email";
			WeeklyForemanPage weeklyForeman = new WeeklyForemanPage(IOS_Driver);
			WeeklyForemanData objweeklyForemanData = new WeeklyForemanData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			login();		
			createWeeklyForeman();			
			weeklyForeman.clickSubmit(WeeklyForemanLocators.submitButton);	
			weeklyForeman.clickElementByName(WeeklyForemanLocators.doneSubmit);	
			weeklyForeman.verReportIsSubmitted(objweeklyForemanData.alertForSubmit);
			weeklyForeman.okButtonTappedForAlertSubmission(objweeklyForemanData.OK);
		    // launch yopmail
		    objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
		    gstrResult = "PASS";
			
		}
}
